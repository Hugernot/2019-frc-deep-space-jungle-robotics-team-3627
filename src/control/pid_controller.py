import math

# pid = PIDController(Kp = 0.1, Ki = 0.1, Kd = 0.1)
# pid.step(input, encoder.get(), loop_time)

class PIDController:
    
    def __init__(self, Kp = 0.1, Ki = 0.1, Kd = 0.1, min_angle = None, max_angle = None, tolerance = 0):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        self.tolerance = tolerance
        self.min_angle = min_angle
        self.max_angle = max_angle

        self.integral = 0
        self.previous_error = 0

    def reset(self):
        self.integral = 0
        self.previous_error = 0

    def update(self, target, actual, delta_time):
        error = (target - actual)

        self.integral = self.integral + error*delta_time
        if delta_time != 0: derivative = (error - self.previous_error) / delta_time # Delta time should never be zero but the sim sometimes gives that value
        else: derivative = 0
        output = (self.Kp*error) + (self.Ki*self.integral) + (self.Kd*derivative)

        if self.max_angle is not None: output = min(output, self.max_angle)
        if self.min_angle is not None: output = max(output, self.min_angle)

        if abs(output) < self.tolerance: output = 0

        self.previous_error = error
        return output
