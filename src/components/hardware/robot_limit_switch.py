from wpilib import DigitalInput

class RobotLimitSwitch(DigitalInput):
    def __init__(self, channel):
        super().__init__(channel)

    def isTriggered(self):
        return not self.get()