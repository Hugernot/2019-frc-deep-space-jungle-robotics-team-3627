import math
from components.abstractions import RobotArm
from control import PIDController

class AngleOrientedArm(RobotArm):

    def __init__(self, robot, angling_motors, encoder=None, potentiometer=None, min_angle=-360, max_angle=360,
        positions = [0], tolerance=10, max_power_down=1, max_power_up = 1, gravity_assist=0, balanced_angle=90):

        super().__init__(robot, angling_motors=angling_motors, encoder=encoder, potentiometer=potentiometer,
            min_angle=min_angle, max_angle=max_angle
        )

        self.positions = positions # Set positions for the motors
        self.position = 0 # Initial position
        # self.pid = PIDController(tolerance=tolerance, min_angle=min_angle, max_angle=max_angle)
        self.pid = PIDController(tolerance=tolerance)
        self.max_power_down = max_power_down
        self.max_power_up = max_power_up
        self.gravity_assist = gravity_assist
        self.balanced_angle = balanced_angle

    def rawRotate(self, speed):
        print("rawRotate:", speed)
        for _, motor in self.angling_motors.items():
            motor.set(speed)

    def rotate(self, speed):
        self.pid.reset()
        if self.angle != None:
            # Arm is going past its max or min points, don't adjust
            if ((speed > 0 and self.angle >= self.max_angle)
            or (speed < 0 and self.angle <= self.min_angle)):
                return

        if speed < 0: self.rawRotate(speed)
        else: self.rawRotate(speed)

    def setAngle(self, target, delta):
        # if self.angle is None:
        #     return
        # # print(
        # #     "------------------------",
        # #     target,
        # #     self.angle,
        # #     delta
        # # )
        # print("in angle_oriented_arm.setAngle():", "current angle", self.positions[self.position], "target angle:", target, "delta_time:", delta)
        # speed = self.pid.update(target, self.angle, delta)/90 # Normalizes from angles to motor power (360 degrees = 100 percent power)
        # assist = -math.sin(math.radians(self.angle - self.balanced_angle))*self.gravity_assist # Gets the cosine of the angle, helping with gravity assist.
        # # print(target, self.angle, speed, assist)
        # speed = max(
        #     min(speed+assist, self.max_power_up), -self.max_power_down
        # )
        # print("in angle_oriented_arm.setAngle(), speed:", speed)
        # self.rawRotate(speed)
        pass

    def nextPosition(self):
        if (self.position+1)+1 > len(self.positions):
            return
        self.position += 1
        # self.setAngle(self.positions[self.position], self.robot.delta_time)

    def prevPosition(self):
        if (self.position-1) < 0:
            return
        self.position -= 1
        # self.setAngle(self.positions[self.position], self.robot.delta_time)

    def anchor(self):
        self.rawRotate(0.1)
