import wpilib, ctre
from components.abstractions import PowerOrientedArm
from components.hardware import RobotLimitSwitch

retracted = 2
activated = 1

class HabitatClimber(PowerOrientedArm):

    is_clamped = False
    is_folded = True

    def __init__(self, robot):
        
        self.solenoid = wpilib.DoubleSolenoid(
            forwardChannel=robot.map.pcm.HAB_CLIMBER_ACTUATE, reverseChannel = robot.map.pcm.HAB_CLIMBER_REVERSE
        )

        self.release_servo = wpilib.Servo(robot.map.pwm.HAB_CLIMBER_RELEASE_SERVO)
        self.limit_switch = RobotLimitSwitch(robot.map.dio.HAB_CLIMBER_LIMIT_SWITCH)

        left_motor = ctre.WPI_VictorSPX(robot.map.can.HAB_CLIMBER_LEFT_ANGLING_MOTOR) 
        right_motor = ctre.WPI_VictorSPX(robot.map.can.HAB_CLIMBER_RIGHT_ANGLING_MOTOR)

        super().__init__(
            robot,
            angling_motors = {
                "left_angling_motor": left_motor,
                "right_angling_motor": right_motor
            },
        )

        del left_motor
        del right_motor

        self.solenoid.set(retracted)
        self.is_clamped = False

    def clamp(self):
        self.solenoid.set(activated)
        self.is_clamped = True

    def releaseClamp(self):
        self.solenoid.set(retracted)
        self.is_clamped = False
        
    def disable(self):
        self.angling_motors['left_angling_motor'].set(0)
        self.angling_motors['right_angling_motor'].set(0)

    def openClamps(self):
        print("Opened CLAMPS")
        self.release_servo.set(180)

    def unfold(self):
        self.rotate(-1)
        if self.limit_switch.isTriggered():
            self.is_folded = False
            self.rotate(0)

    def operate(self):
        if self.robot.controller.right_bumper_pressed and self.robot.controller.left_bumper_pressed:
            self.openClamps()

        if self.robot.controller.a_just_pressed:
            if self.is_clamped: self.releaseClamp()
            else: self.clamp()

        self.rotate(
            self.robot.controller.left_trigger - self.robot.controller.right_trigger
        )

